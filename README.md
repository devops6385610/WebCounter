# Webcounter

Simple Python Webcounter with redis server

## Build
docker build -t faccardoso/webcounter:1.0.0 .

## Dependencies
docker run -d  --name redis --rm redis:alpine

## Run
docker run -d --rm -p80:5000 --name webcounter --link redis -e REDIS_URL=redis faccardoso/webcounter:1.0.0

### Install gitlab-runner
    
    sudo apk add gitlab-runner

### Gitlab register

gitlab-runner register -n \
--url https://gitlab.com/ \
--executor shell \
--description "develop-node" \
--tag-list "develop" \
--registration-token GR13489418VhMqx7TxN_anyZ8Z_kH

### Run runner
    
    gitlab-runner run
